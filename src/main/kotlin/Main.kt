class Problema(enunc: String, inpPub: String, outPub: Int, inpPr: String, outPr: Int) {
    var enunciat = enunc
    var inputPublic = inpPub
    var outputPublic = outPub
    var inputPrivat = inpPr
    var outputPrivat = outPr
    var resolt = false
    var sortir = false
    var intents = 0
    var intentsUsuari = ""
}


fun main() {
    /* Creació dels problemes passant-li els valors de cadascun d'ells a la classe Problema
    dels inputs i outputs tan públics com privats i l'enunciat*/
    val problema1 = Problema("A partir dels dos següents números, introdueix el resultat de la suma", "3 i 2", 5, "7 i 17", 24)
    val problema2 = Problema("Donats els següents números, calcula la mitja d'aquests", "20, 59, 34, 27 i 20", 32, "61, 54, 87, 30, 48, 63, 37 i 4", 48)
    val problema3 = Problema("Calcula el factorial", "4", 24, "6", 720)
    val problema4 = Problema("Donada la següent equació, calcula el valor de la X", "2(1+2X)=10", 2, "-2(3X-2)=-2", 1)
    val problema5 = Problema("Donada la següent operació matemàtica calcula el seu resultat final, si dona decimals posa la part entera", "5*7/3+37*(5*9-30)", 566, "9/3*51√(5)*6", 2052)
    println("Benvingut al JutgITB")


    //PROBLEMA 1
    /*Mostra els jocs de proves i l'enunciat*/
    println("PROBLEMA 1")
    println("Input del joc de proves públic:")
    println(problema1.inputPublic)
    println("Output del joc de proves públic:")
    println(problema1.outputPublic)
    println(problema1.enunciat)
    println(problema1.inputPrivat)

    // Li pregunta si vol resoldre el problema i li demana que introdueixi 1 (resoldre)o 0 (passar al següent)
    println("Vols resoldre aquest problema? Introdueix un 1 per resoldre'l o un 0 per passar al següent")
    var resoldre = 2
    while (resoldre!=0 && resoldre!=1){
        resoldre = readln().toInt()
    }

    // Si tria resoldre el problema li torna a mostrar l'enunciat i els jocs de proves
    if (resoldre==1) {
        println()
        println("PROBLEMA 1")
        println("Input del joc de proves públic:")
        println(problema1.inputPublic)
        println("Output del joc de proves públic:")
        println(problema1.outputPublic)
        println(problema1.enunciat)
        println(problema1.inputPrivat)
        do {
            println("Introdueix el resultat o un 0 per sortir i passar al següent problema")
            // Si ja ha fallat en un intent anterior li mostra els resultats que havia introduït anteriorment
            if (problema1.intents>=1){
                println("Els intents incorrectes que ja has probat son: " + problema1.intentsUsuari)
            }
            var resultatUsuari = readln().toInt()
            // Si l'encerta li informa i passa la variable .resolt a true per a sortir del do while
            if (resultatUsuari == problema1.outputPrivat){
                println("HAS ENCERTAT EL RESULTAT!!")
                problema1.resolt=true
            }
            // Si en qualsevol moment vol sortir introduïnt un 0 ho pot fer i passa la variable .sortir a true per sortir del so while
            else if (resultatUsuari==0){
                println("Has triat sortir")
                problema1.sortir=true
            }
            else{
                // Si falla li informa i afegeix el resultat incorrecte a un string per informar-li de tots els intents incorrectes que ha fet anteriorment
                println("Resultat incorrecte, torna a intentar-ho")
                problema1.intentsUsuari += resultatUsuari.toString() + "  "
            }
            // Suma 1 intent
            problema1.intents++
            // Mentre que .resolt i .sortir siguin false no podrà sortir del do while
        } while (problema1.resolt==false && problema1.sortir==false)
    }
    else{
        println("Has triat passar al següent problema")
    }


    repeat(5) {
        println()
    }

    //PROBLEMA 2
    println("PROBLEMA 2")
    println("Input del joc de proves públic:")
    println(problema2.inputPublic)
    println("Output del joc de proves públic:")
    println(problema2.outputPublic)
    println(problema2.enunciat)
    println(problema2.inputPrivat)

    println("Vols resoldre aquest problema? Introdueix un 1 per resoldre'l o un 0 per passar al següent")
    resoldre = 2
    while (resoldre!=0 && resoldre!=1){
        resoldre = readln().toInt()
    }

    if (resoldre==1) {
        println()
        println("PROBLEMA 2")
        println("Input del joc de proves públic:")
        println(problema2.inputPublic)
        println("Output del joc de proves públic:")
        println(problema2.outputPublic)
        println(problema2.enunciat)
        println(problema2.inputPrivat)
        do {
            println("Introdueix el resultat o un 0 per sortir i passar al següent problema")
            if (problema2.intents>=1){
                println("Els intents incorrectes que ja has probat son: " + problema2.intentsUsuari)
            }
            var resultatUsuari = readln().toInt()
            if (resultatUsuari == problema2.outputPrivat){
                println("HAS ENCERTAT EL RESULTAT!!")
                problema2.resolt=true
            }
            else if (resultatUsuari==0){
                println("Has triat sortir")
                problema2.sortir=true
            }
            else{
                println("Resultat incorrecte, torna a intentar-ho")
                problema2.intentsUsuari += resultatUsuari.toString() + "  "
            }
            problema2.intents++
        } while (problema2.resolt==false && problema2.sortir==false)
    }
    else{
        println("Has triat passar al següent problema")
    }


    repeat(5) {
        println()
    }

    //PROBLEMA 3
    println("PROBLEMA 3")
    println("Input del joc de proves públic:")
    println(problema3.inputPublic)
    println("Output del joc de proves públic:")
    println(problema3.outputPublic)
    println(problema3.enunciat)
    println(problema3.inputPrivat)

    println("Vols resoldre aquest problema? Introdueix un 1 per resoldre'l o un 0 per passar al següent")
    resoldre = 2
    while (resoldre!=0 && resoldre!=1){
        resoldre = readln().toInt()
    }

    if (resoldre==1) {
        println()
        println("PROBLEMA 3")
        println("Input del joc de proves públic:")
        println(problema3.inputPublic)
        println("Output del joc de proves públic:")
        println(problema3.outputPublic)
        println(problema3.enunciat)
        println(problema3.inputPrivat)
        do {
            println("Introdueix el resultat o un 0 per sortir i passar al següent problema")
            if (problema3.intents>=1){
                println("Els intents incorrectes que ja has probat son: " + problema3.intentsUsuari)
            }
            var resultatUsuari = readln().toInt()
            if (resultatUsuari == problema3.outputPrivat){
                println("HAS ENCERTAT EL RESULTAT!!")
                problema3.resolt=true
            }
            else if (resultatUsuari==0){
                println("Has triat sortir")
                problema3.sortir=true
            }
            else{
                println("Resultat incorrecte, torna a intentar-ho")
                problema3.intentsUsuari += resultatUsuari.toString() + "  "
            }
            problema3.intents++
        } while (problema3.resolt==false && problema3.sortir==false)
    }
    else{
        println("Has triat passar al següent problema")
    }


    repeat(5) {
        println()
    }

    //PROBLEMA 4
    println("PROBLEMA 4")
    println("Input del joc de proves públic:")
    println(problema4.inputPublic)
    println("Output del joc de proves públic:")
    println(problema4.outputPublic)
    println(problema4.enunciat)
    println(problema4.inputPrivat)

    println("Vols resoldre aquest problema? Introdueix un 1 per resoldre'l o un 0 per passar al següent")
    resoldre = 2
    while (resoldre!=0 && resoldre!=1){
        resoldre = readln().toInt()
    }

    if (resoldre==1) {
        println()
        println("PROBLEMA 4")
        println("Input del joc de proves públic:")
        println(problema4.inputPublic)
        println("Output del joc de proves públic:")
        println(problema4.outputPublic)
        println(problema4.enunciat)
        println(problema4.inputPrivat)
        do {
            println("Introdueix el resultat o un 0 per sortir i passar al següent problema")
            if (problema4.intents>=1){
                println("Els intents incorrectes que ja has probat son: " + problema4.intentsUsuari)
            }
            var resultatUsuari = readln().toInt()
            if (resultatUsuari == problema4.outputPrivat){
                println("HAS ENCERTAT EL RESULTAT!!")
                problema4.resolt=true
            }
            else if (resultatUsuari==0){
                println("Has triat sortir")
                problema4.sortir=true
            }
            else{
                println("Resultat incorrecte, torna a intentar-ho")
                problema4.intentsUsuari += resultatUsuari.toString() + "  "
            }
            problema4.intents++
        } while (problema4.resolt==false && problema4.sortir==false)
    }
    else{
        println("Has triat passar al següent problema")
    }


    repeat(5) {
        println()
    }

    //PROBLEMA 5
    println("PROBLEMA 5")
    println("Input del joc de proves públic:")
    println(problema5.inputPublic)
    println("Output del joc de proves públic:")
    println(problema5.outputPublic)
    println(problema5.enunciat)
    println(problema5.inputPrivat)

    println("Vols resoldre aquest problema? Introdueix un 1 per resoldre'l o un 0 per passar al següent")
    resoldre = 2
    while (resoldre!=0 && resoldre!=1){
        resoldre = readln().toInt()
    }

    if (resoldre==1) {
        println()
        println("PROBLEMA 5")
        println("Input del joc de proves públic:")
        println(problema5.inputPublic)
        println("Output del joc de proves públic:")
        println(problema5.outputPublic)
        println(problema5.enunciat)
        println(problema5.inputPrivat)
        do {
            println("Introdueix el resultat o un 0 per sortir i passar al següent problema")
            if (problema5.intents>=1){
                println("Els intents incorrectes que ja has probat son: " + problema5.intentsUsuari)
            }
            var resultatUsuari = readln().toInt()
            if (resultatUsuari == problema5.outputPrivat){
                println("HAS ENCERTAT EL RESULTAT!!")
                problema5.resolt=true
            }
            else if (resultatUsuari==0){
                println("Has triat sortir")
                problema5.sortir=true
            }
            else{
                println("Resultat incorrecte, torna a intentar-ho")
                problema5.intentsUsuari += resultatUsuari.toString() + "  "
            }
            problema5.intents++
        } while (problema5.resolt==false && problema5.sortir==false)
    }
    else{
        println("Has triat passar al següent problema")
    }


    repeat(5) {
        println()
    }

    println("RESULTATS FINALS")
    // PROBLEMA 1
    if (problema1.resolt==true){
        println("S'ha aconseguit resoldre el problema 1 en un total de ${problema1.intents} intents")
    }
    else println("No s'ha aconseguit resoldre el problema 1")

    // PROBLEMA 2
    if (problema2.resolt==true){
        println("S'ha aconseguit resoldre el problema 2 en un total de ${problema2.intents} intents")
    }
    else println("No s'ha aconseguit resoldre el problema 2")

    // PROBLEMA 3
    if (problema3.resolt==true){
        println("S'ha aconseguit resoldre el problema 3 en un total de ${problema3.intents} intents")
    }
    else println("No s'ha aconseguit resoldre el problema 3")

    // PROBLEMA 4
    if (problema4.resolt==true){
        println("S'ha aconseguit resoldre el problema 4 en un total de ${problema4.intents} intents")
    }
    else println("No s'ha aconseguit resoldre el problema 4")

    // PROBLEMA 5
    if (problema5.resolt==true){
        println("S'ha aconseguit resoldre el problema 5 en un total de ${problema5.intents} intents")
    }
    else println("No s'ha aconseguit resoldre el problema 5")
}